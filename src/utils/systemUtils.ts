// import {v4 as uuidv4} from 'uuid'
/**
 * 移除undefine的属性
 */
export const removeObjEmpty = (obj: Object) => {
  const newObj = {}
  Object.keys(obj).forEach((key) => {
    console.log(`${key}: ${obj[key]}`)
    if (obj[key] != undefined) {
      if (typeof obj[key] === 'string') {
        if (obj[key].length > 0) {
          newObj[key] = obj[key]
        }
      } else {
        newObj[key] = obj[key]
      }
    }
  })
  return newObj
}

/**
 * 移除undefine的属性
 */
export const removeObjUndefined = (obj: Object) => {
  const newObj = {}
  Object.keys(obj).forEach((key) => {
    console.log(`${key}: ${obj[key]}`)
    if (obj[key] != undefined) {
      newObj[key] = obj[key]
    }
  })
  return newObj
}

/**
 * 把所有的属性设置为undefined
 * @param obj
 */
export const setObjToUndefined = (obj: any) => {
  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      obj[key] = undefined
    }
  }
}

/**
 * 获取uuid
 */
// export const getUUID = () => {
//   return uuidv4()
// }

/**
 * 是否null
 * @param str
 */
export const isEmpty = (str: any) => {
  if (typeof str === 'string') {
    return !str
  }
  if (str == null || str == undefined) {
    return true
  } else {
    return false
  }
}
