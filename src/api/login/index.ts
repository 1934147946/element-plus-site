import request from '@/axios'
import type { UserType, LoginParamsType, UserInfoType, RefreshTokenParamsType, GetUserListType, GetUserListParamsType } from './types'
import { GET_USER_LIST_API } from '@/api/config'

interface RoleParams {
  roleName: string
}

export const loginApi = (data: LoginParamsType): Promise<IResponse<UserType>> => {
  return request.post({ url: '/user/login', data })
}

export const refreshTokenApi = (params: RefreshTokenParamsType): Promise<IResponse<UserInfoType>> => {
  return request.get({ url: '/user/refreshToken', params })
}

export const getUserListApi = (params: GetUserListParamsType): Promise<IResponse<GetUserListType>> => {
  return request.get({ url: GET_USER_LIST_API, params })
}

export const loginOutApi = (): Promise<IResponse> => {
  return request.get({ url: '/mock/user/loginOut' })
}

export const getAdminRoleApi = (params: RoleParams): Promise<IResponse<AppCustomRouteRecordRaw[]>> => {
  return request.get({ url: '/mock/role/list', params })
}

export const getTestRoleApi = (params: RoleParams): Promise<IResponse<string[]>> => {
  return request.get({ url: '/mock/role/list2', params })
}
