export type UserInfoType = {
  username: string
  password: string
  role: string
  roleId: string
  permissions: string | string[]
  id?: number
  type?: number
  headUrl?: string
  nick?: string
  email?: string
  token?: string
  createtime?: number
}

export type RoleType = {
  id?: number
  userId?: number
  path?: string
  component?: string
  redirect?: string
  name?: string
  metaTitle?: string
  metaIcon?: string
  metaAlwaysShow?: number
  parentId?: number
  createtime?: number
  childRoles?: RoleType[]
}

export type UserLoginType = {
  username: string
  password: string
}

export type UserType = {
  userInfo?: UserInfoType
  // roleList?: RoleType[]
  roleList?: AppCustomRouteRecordRaw[]
}

export type LoginParamsType = {
  username: string
  password: string
}

export type RefreshTokenParamsType = {
  loseToken: string
}

export type GetUserListType = {
  list?: Array<UserInfoType>
  totalSize?: number
}

export type GetUserListParamsType = {
  email?: string
}
