import request from '@/axios'
import type { QueryRoleType, QueryRoleParamsType, RoleType, AddRoleParamsType, QueryRoleFrameworkType, DeleteRoleParamsType, SortRoleParamsType } from './types'
import { QUERY_ROLE_API, ADD_ROLE_API, QUERY_ROLE_FRAMEWORK_API, QUERY_ROLE_DETAIL_API, UPDATE_ROLE_API, DELETE_ROLE_API, SORT_ROLE_API } from '@/api/config'

export const queryRoleApi = (params: QueryRoleParamsType): Promise<IResponse<QueryRoleType>> => {
  return request.get({ url: QUERY_ROLE_API, params })
}

export const queryRoleFrameworkApi = (params: QueryRoleParamsType): Promise<IResponse<QueryRoleFrameworkType>> => {
  return request.get({ url: QUERY_ROLE_FRAMEWORK_API, params })
}

export const addRoleApi = (data: AddRoleParamsType): Promise<IResponse<RoleType>> => {
  return request.post({ url: ADD_ROLE_API, data })
}

export const queryRoleDetailApi = (params: QueryRoleParamsType): Promise<IResponse<RoleType>> => {
  return request.get({ url: QUERY_ROLE_DETAIL_API, params })
}

export const updateRoleApi = (data: AddRoleParamsType): Promise<IResponse<string>> => {
  return request.put({ url: UPDATE_ROLE_API, data })
}

export const deleteRoleApi = (params: DeleteRoleParamsType): Promise<IResponse<string>> => {
  return request.delete({ url: DELETE_ROLE_API, params })
}

export const sortRoleApi = (data: Array<SortRoleParamsType>): Promise<IResponse<string>> => {
  return request.put({ url: SORT_ROLE_API, data })
}
