export type RoleType = {
  id?: number
  userId?: number
  path?: string
  component?: string
  redirect?: string
  name?: string
  parentId?: number
  meta?: MetaType
  children?: Array<RoleType>
  createtime?: number
  isLogin?: boolean
  isDel?: boolean
}

export type QueryRoleType = {
  list?: Array<RoleType>
  totalSize?: number
}

export type QueryRoleParamsType = {
  name?: string
  userId?: number
  id?: number
  parentId?: number
  isDel?: boolean
  excludeIds?: Array<number>
  isLogin?: boolean
  pageNum?: number
  pageSize?: number
}

export type MetaType = {
  hidden?: boolean
  alwaysShow?: boolean
  title?: string
  icon?: string
  noCache?: boolean
  breadcrumb?: boolean
  affix?: boolean
  activeMenu?: boolean
  noTagsView?: boolean
  followAuth?: string
  canTo?: boolean
  permission?: string
}

export type AddRoleParamsType = {
  userId?: number
  path?: string
  component?: string
  redirect?: string
  name?: string
  parentId?: number
  isLogin?: boolean
  isDel?: boolean
  meta?: MetaType
}

export type QueryRoleFrameworkType = {
  totalSize?: number
  list?: AppCustomRouteRecordRaw[]
}

export type DeleteRoleParamsType = {
  ids: Array<number>
}

export type SortRoleParamsType = {
  sort: number
  id: number
}
