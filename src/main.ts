import 'vue/jsx'

// 引入unocss
import '@/plugins/unocss'

// 导入全局的svg图标
import '@/plugins/svgIcon'

// 初始化多语言
import { setupI18n } from '@/plugins/vueI18n'

// 引入状态管理
import { setupStore } from '@/store'

// 全局组件
import { setupGlobCom } from '@/components'

// 引入element-plus
import { setupElementPlus } from '@/plugins/elementPlus'

// 引入全局样式
import '@/styles/index.less'

// 引入动画
import '@/plugins/animate.css'

// 路由
import { setupRouter } from './router'

import { createApp } from 'vue'

import App from './App.vue'

import './permission'

import { queryRoleFrameworkApi } from '@/api/role'
import { QueryRoleParamsType } from '@/api/role/types'
import { useUserStore } from '@/store/modules/user'




const requestQueryRoleFrameworkApi = async () => {

  const userStore = useUserStore()
  const params: QueryRoleParamsType = {
    isLogin: !!userStore.getUserInfo ? undefined : false
  }

  const res = await queryRoleFrameworkApi(params)

  if (res.code == 1) {
    if (res.data.list) {
      userStore.setRoleRouters(res.data.list!)
    }
  }
}



// 创建实例
const setupAll = async () => {
  const app = createApp(App)

  await setupI18n(app)

  setupStore(app)

  setupGlobCom(app)

  setupElementPlus(app)

  setupRouter(app)

  await requestQueryRoleFrameworkApi()

  app.mount('#app')
}

setupAll()
