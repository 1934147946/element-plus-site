# Element-Plus-Site

#### 介绍
element-plus-site 是用于搭建网站的基本功能，主要根据权限来开启需要登录还是不需要登录的页面

#### 软件架构

适合如下架构的应用开发的基础架构

![应用架构](https://gitee.com/1934147946/resource/raw/master/resource/7a70d5fa-ff40-4871-b640-3022ab0e4475.png)



#### 安装运行教程

1.  下载依赖

```
pnpm install
```

2.  开发模式运行

```
pnpm dev
```

#### 设置环境

1.  主要文件

```
.env.base
.env.dev
.env.prod
.env.test
.env.gitee
```

2.  根据你后台环境设置

```
# 环境
VITE_NODE_ENV=production

# 接口前缀
VITE_API_BASE_PATH=/api

# 打包路径
VITE_BASE_PATH=/dist-dev/

# 是否删除debugger
VITE_DROP_DEBUGGER=false

# 是否删除console.log
VITE_DROP_CONSOLE=false

# 是否sourcemap
VITE_SOURCEMAP=true

# 输出路径
VITE_OUT_DIR=dist-dev

# 标题
VITE_APP_TITLE=BTool

# 是否包分析
VITE_USE_BUNDLE_ANALYZER=false

# 是否全量引入element-plus样式
VITE_USE_ALL_ELEMENT_PLUS_STYLE=false

# 是否开启mock
VITE_USE_MOCK=false

# 是否切割css
VITE_USE_CSS_SPLIT=true

# 是否使用在线图标
VITE_USE_ONLINE_ICON=true
```

#### 后台权限数据库表

1. role 角色表

![角色表](https://gitee.com/1934147946/resource/raw/master/resource/da1669e6-86cb-4ca5-ab66-a7aefe7904af.png)

2. meta 菜单表

![菜单表](https://gitee.com/1934147946/resource/raw/master/resource/77d8437f-eba1-4496-80be-3c36f7bbc5a0.png)

#### 后台API

![后台api](https://gitee.com/1934147946/resource/raw/master/resource/32ba3265-28dc-43dc-80cd-ecaa32bec0fb.png)

#### 前端路由

1.  添加路由流程

```
页面请放在Views文件夹下
```

2.  路由如何传递参数

```
import { useRouter } from 'vue-router'

const { push } = useRouter()

push('/role/addRole?id=' + id)
```

3.  路由如何获取传递过来的参数

```
import { useRoute } from 'vue-router'

const { query } = useRoute()

if (!!query.id) {
  title.value = '修改路由'
} else {
  title.value = '添加路由'
}


```

4.  如何设置tag的标题

```
import { useTagsView } from '@/hooks/web/useTagsView'


const { setTitle } = useTagsView()


setTitle(title.value)
```

#### 前端API

1、请求API步骤

```
1、在api中config.ts文件中添加接口地址

2、在api中建立api类型文件夹，如：user

3、在类型文件夹下建立接口文件以及类型，如：index.ts，types.ts

4、在页面中使用api
```

#### 效果

![登录](https://gitee.com/1934147946/resource/raw/master/resource/dff93d00-cb89-474f-b784-c79496dc2091.png)

![路由](https://gitee.com/1934147946/resource/raw/master/resource/64c9bdf2-851b-4fb7-a87f-fa62ba2dca9c.png)

![修改](https://gitee.com/1934147946/resource/raw/master/resource/75a4eb81-5705-4a5d-9886-bbc1d4c904ee.png)
